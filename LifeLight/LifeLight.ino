#include <Adafruit_NeoPixel.h>    

int ledPin = 6;                // choose the pin for the LED
int inputPin = 2;               // choose the input pin (for PIR sensor)
int pirState = LOW;             // we start, assuming no motion detected
int val = 0;                    // variable for reading the pin status

int counterForMotion = 0;  
int MAXCOUNTER = 15;      

int fadeInTimePerColorInMs = 5000;
int greenInMS = fadeInTimePerColorInMs;   
int yellowInMS = fadeInTimePerColorInMs;   
int orangeInMS = fadeInTimePerColorInMs;   
int redInMS = fadeInTimePerColorInMs;

int LED_COUNT = 60;
Adafruit_NeoPixel strip(LED_COUNT, ledPin, NEO_GRBW  + NEO_KHZ800);
// Übersetzt von RGB in GRB für den LED-Strip

// Farbwerte
uint32_t rot = strip.Color(255, 0, 0, 0);
uint32_t gruen = strip.Color(0, 255, 0, 0);
uint32_t blau = strip.Color(0, 255, 255, 0);
uint32_t weiss = strip.Color(0, 0, 0, 255);

 
void setup() {
  pinMode(ledPin, OUTPUT);      // declare LED as output
  pinMode(inputPin, INPUT);     // declare sensor as input

  strip.begin();
  strip.show(); //Initialiize all pixels to OFF
  Serial.begin(9600);

}

 
void loop(){
  //colorFade();

  checkMovement();

}

void checkMovement(){

  val = digitalRead(inputPin);  // read input value
  if (val == HIGH) {            // check if the input is HIGH
    Serial.println("Motion detected!");
    if (pirState == LOW) {
      pirState = HIGH;  
      fadeIn(); // turn the light on 
    }
    counterForMotion = MAXCOUNTER;
    Serial.println(counterForMotion);
    delay(1 * 1000); // 1 second delay
    
  } else {
    if (pirState == HIGH){
      counterForMotion -= 1;
      Serial.println(counterForMotion);
      delay(1 * 1000); // 1 second delay
      
      if(counterForMotion < 1){
        // we have just turned off
        Serial.println("Motion ended!");
        // We only want to print on the output change, not state
        pirState = LOW;
        strip.clear();
        strip.show();
      }
    }
 }
 }

void fadeIn() {
    for( int i = 0; i < 60; i++ ) {
       for( int j = 0; j < 60; j++ ) {
          strip.setPixelColor(j, 0, 0, i*2,i*4);
      }
       strip.show();
       delay(100);// 100 ms delay
    }
    strip.clear();
}

void colorFade(){
  // Fade in green
    for( int i = 0; i < 64; i++ ) {
      for( int j = 0; j < 60; j++ ) {
        strip.setPixelColor(j, 0, i*4, 0, 0);
      }
      strip.show();
      delay(greenInMS/64);// delay
    }

    // Fade in yellow
    for( int i = 0; i < 64; i++ ) {
      for( int j = 0; j < 60; j++ ) {
        strip.setPixelColor(j, i*2, 255, 0, 0);
      }
      strip.show();
      delay(yellowInMS/64);// delay
    }

    // Fade in orange
    for(int i = 64; i < 128; i++ ) {
      for( int j = 0; j < 60; j++ ) {
        strip.setPixelColor(j, i*2, 255, 0, 0);
      }
      strip.show();
      delay(orangeInMS/64);// delay
    }

    
    // Fade in red
    for( int i = 64; i > 0; i-- ) {
      for( int j = 0; j < 60; j++ ) {
        strip.setPixelColor(j, 255, i*4-1, 0, 0);
      }
      strip.show();
      delay(redInMS/64);// delay
    }

  // Blink in red
   for( int a = 0; a < 30; a++ ) {
      strip.clear();
      strip.show();
      delay(100);
      strip.fill(rot);
      strip.show();
      delay(100);
    }

  strip.clear();
  strip.show();
  delay(5000);
}
